# TacoLoco

TacoLoco is a Spring Boot application to manage orders.
All data is located in memory within a h2 database.

Functionality includes:
- Create
- Read (List and Single)
- Update

## Prerequisites

- Java 8 (https://www.oracle.com/mx/java/technologies/javase/javase8-archive-downloads.html)
- Postman (https://www.postman.com/downloads/)

# Build

1. Open a command line in root directory
2. Execute:

> mvnw clean install

### Notes:

if you want to skip test during build you can execute as follows:
> mvnw clean install -Dmaven.test.skip=true

# Run

1. Open a command line in root directory
2. Execute:

> java -jar target/tacoloco-0.0.1-SNAPSHOT.jar

### Notes:

Application by default runs in port 8080.

# Execute Unit Tests

1. Open a command line in root directory
2. Execute:

> mvnw clean test

### Notes:

If you want to run a specific test you may do so by doing:
> mvnw -Dtest=classname#testname test

where 'classname' is the name of the class and 'testname' is the name of the test, example:

> mvnw -Dtest=TacoLocoApplicationTests#Client_saveClient_NotNull test

# Postman Tests

Requests collections are inside postman folder in file:
> TacoLoco.postman_collection.json

1. Click on import
2. Click on upload files and select the file
3. Click on import