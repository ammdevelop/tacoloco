package com.demo.tacoloco.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;

public abstract class ValidationHandler {

    @ExceptionHandler({MethodArgumentNotValidException.class, ConstraintViolationException.class})
    ResponseEntity<String> handleValidationExceptions(MethodArgumentNotValidException e){
        return new ResponseEntity<>("The following exceptions occurred during validations: " + e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
