package com.demo.tacoloco.controller;

import com.demo.tacoloco.entity.Orders;
import com.demo.tacoloco.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/order")
@Slf4j
@Validated
public class OrderController {

    @Autowired
    public OrderService orderService;

    @PostMapping
    public ResponseEntity<?> placeOrder(@Valid @RequestBody Orders order){
        if(order == null){
            return new ResponseEntity<>("Order is empty", HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(orderService.placeOrder(order));
    }

    @GetMapping
    public ResponseEntity<List<Orders>> getAllOrders(){
        return ResponseEntity.ok(orderService.getAllOrders());
    }

    @GetMapping("/find/{orderId}")
    public ResponseEntity<?> getOrderById(@PathVariable("orderId") Long orderId){
        if(orderId == null){
            return new ResponseEntity<>("Order is empty", HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(orderService.findOrderById(orderId));
    }

    @PutMapping("/update/{orderId}")
    public ResponseEntity<?> updateOrderById(@Valid @RequestBody Orders order, @PathVariable("orderId") Long orderId){
        if(order == null || orderId == null){
            return new ResponseEntity<>("Order is empty", HttpStatus.BAD_REQUEST);
        }
        boolean created = orderService.updateOrderById(orderId, order) > 0;
        return new ResponseEntity<>(created ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}
