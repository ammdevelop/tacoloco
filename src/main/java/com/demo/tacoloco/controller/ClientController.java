package com.demo.tacoloco.controller;

import com.demo.tacoloco.entity.Client;
import com.demo.tacoloco.service.ClientService;
import com.demo.tacoloco.util.ValidationHandler;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/client")
@Validated
@Slf4j
public class ClientController extends ValidationHandler {

    private final ClientService clientService;

    public ClientController(ClientService clientService){
        this.clientService = clientService;
    }

    @PostMapping
    public ResponseEntity<?> saveClient(@Valid @RequestBody Client client){
        if(client == null){
            return new ResponseEntity<>("Client is empty", HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(clientService.saveClient(client));
    }

    @GetMapping("/find/{clientId}")
    public ResponseEntity<?> findClientById(@PathVariable("clientId") Long clientId){
        if(clientId == null){
            return new ResponseEntity<>("Client id is empty", HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(clientService.findClientById(clientId));
    }

    @PutMapping("/update/{clientId}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Client name is empty"),
    })
    public ResponseEntity<?> updateClientName(@PathVariable("clientId") Long clientId, @RequestParam String name){
        if(StringUtils.isEmpty(name)){
            return new ResponseEntity<>("Client name is empty", HttpStatus.BAD_REQUEST);
        }
        boolean created = clientService.updateClientName(clientId, name) > 0;
        return new ResponseEntity<>(created ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @GetMapping
    public ResponseEntity<List<Client>> getAllClients(){
        return ResponseEntity.ok(clientService.getAllClients());
    }

}
