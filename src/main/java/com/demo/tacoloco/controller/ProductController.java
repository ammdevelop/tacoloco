package com.demo.tacoloco.controller;

import com.demo.tacoloco.entity.Product;
import com.demo.tacoloco.service.ProductService;
import com.demo.tacoloco.util.ValidationHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/product")
@Validated
@Slf4j
public class ProductController extends ValidationHandler {

    private final ProductService productService;

    public ProductController(ProductService productService){
        this.productService = productService;
    }

    @PostMapping
    public ResponseEntity<?> saveProduct(@Valid @RequestBody Product product){
        if(product == null){
            return new ResponseEntity<>("Product is empty", HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(productService.saveProduct(product));
    }

    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts(){
        return ResponseEntity.ok(productService.getAllProducts());
    }
}
