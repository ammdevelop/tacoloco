package com.demo.tacoloco.service;

import com.demo.tacoloco.entity.Product;
import com.demo.tacoloco.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    public ProductRepository productRepository;

    public Product findProductById(long productId){
        return productRepository.findProductById(productId);
    }

    @Transactional
    public Product saveProduct(Product product){
        return productRepository.save(product);
    }

    public List<Product> getAllProducts(){
        return productRepository.findAll();
    }
}
