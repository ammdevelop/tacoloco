package com.demo.tacoloco.service;

import com.demo.tacoloco.entity.Client;
import com.demo.tacoloco.entity.Detail;
import com.demo.tacoloco.entity.Orders;
import com.demo.tacoloco.repository.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    public OrdersRepository ordersRepository;
    @Autowired
    public ClientService clientService;
    @Autowired
    public ProductService productService;
    @Autowired
    public DetailService detailService;
    @Autowired
    public OrderService orderService;

    @Transactional
    public Orders placeOrder(Orders order){
        return ordersRepository.save(parseOrder(order));
    }

    public List<Orders> getAllOrders(){
        return ordersRepository.findAll();
    }

    public Orders findOrderById(long orderId){
        Orders order = ordersRepository.findOrdersById(orderId);
        List<Detail> detailList = detailService.findOrderDetailsByOrderId(order.getId());
        order.setDetail(detailList);
        return order;
    }

    public int updateOrderById(long orderId, Orders order){
        Orders orders = ordersRepository.findOrdersById(orderId);
        detailService.deleteByOrderId(orderId);
        orders.setDetail(order.getDetail());
        orders.setClient(order.getClient());
        ordersRepository.save(parseOrder(orders));
        return 1;
    }

    private Orders parseOrder(Orders order){
        Client client = clientService.findClientById(order.getClient().getClient_id());
        order.setClient(client);
        order.setTimestamp(new Date());
        order.getDetail().forEach(detail -> detail.setProduct(productService.findProductById(detail.getProduct().getProduct_id())));
        order.getDetail().forEach(orders -> orders.setOrder(order));
        return order;
    }
}
