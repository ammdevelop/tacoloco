package com.demo.tacoloco.service;

import com.demo.tacoloco.entity.Client;
import com.demo.tacoloco.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ClientService {

    @Autowired
    public ClientRepository clientRepository;

    @Transactional
    public Client saveClient(Client client){
        return clientRepository.save(client);
    }

    @Transactional
    public int updateClientName(long clientId, String clientName){
        return clientRepository.updateClientName(clientName, clientId);
    }

    public Client findClientById(long id) {
        return clientRepository.getClientById(id);
    }

    public List<Client> getAllClients(){
        return clientRepository.getAllClients();
    }
}
