package com.demo.tacoloco.service;

import com.demo.tacoloco.entity.Detail;
import com.demo.tacoloco.repository.DetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class DetailService {

    @Autowired
    public DetailRepository detailRepository;

    @Transactional
    public void deleteByOrderId(long orderId){
        detailRepository.deleteAllByOrderId(orderId);
    }

    public List<Detail> findOrderDetailsByOrderId(long orderId){
        return detailRepository.findOrderDetailsByOrderId(orderId);
    }
}
