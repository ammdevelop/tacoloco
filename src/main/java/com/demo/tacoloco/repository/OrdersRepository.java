package com.demo.tacoloco.repository;

import com.demo.tacoloco.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdersRepository extends JpaRepository<Orders, Long> {
    Orders findOrdersById(long orderId);
}