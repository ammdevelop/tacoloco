package com.demo.tacoloco.repository;

import com.demo.tacoloco.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("SELECT p FROM Product p WHERE p.product_id = :productId ")
    public Product findProductById(long productId);
}