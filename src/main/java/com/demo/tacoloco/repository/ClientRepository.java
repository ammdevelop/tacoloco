package com.demo.tacoloco.repository;

import com.demo.tacoloco.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    @Modifying
    @Query("UPDATE Client c SET c.name = :name WHERE c.client_id = :id")
    public int updateClientName(@Param("name") String name, @Param("id") long id);

    @Query("SELECT c FROM Client c")
    public List<Client> getAllClients();

    @Query("SELECT c FROM Client c where c.client_id = :id")
    public Client getClientById(@Param("id") long id);
}
