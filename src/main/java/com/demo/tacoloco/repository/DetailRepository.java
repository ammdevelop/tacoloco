package com.demo.tacoloco.repository;

import com.demo.tacoloco.entity.Detail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DetailRepository extends JpaRepository<Detail, Long> {
    @Query("SELECT od FROM Detail od WHERE od.order.id = :orderId ")
    public List<Detail> findOrderDetailsByOrderId(@Param("orderId") long orderId);

    @Modifying
    @Query("DELETE FROM Detail od WHERE od.order.id = :orderId ")
    public void deleteAllByOrderId(long orderId);

}