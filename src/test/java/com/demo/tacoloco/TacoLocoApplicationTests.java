package com.demo.tacoloco;

import com.demo.tacoloco.entity.Client;
import com.demo.tacoloco.entity.Detail;
import com.demo.tacoloco.entity.Orders;
import com.demo.tacoloco.entity.Product;
import com.demo.tacoloco.service.ClientService;
import com.demo.tacoloco.service.OrderService;
import com.demo.tacoloco.service.ProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootTest
class TacoLocoApplicationTests {

	@Autowired
	public OrderService orderService;
	@Autowired
	public ClientService clientService;
	@Autowired
	public ProductService productService;

	@Test
	void Client_saveClient_NotNull() {
		Client client = createNewClient();
		Assertions.assertNotNull(clientService.findClientById(client.getClient_id()));
	}

	@Test
	void Client_saveClient_VerifyName() {
		Client client = createNewClient();
		Assertions.assertEquals(client.getName(), clientService.findClientById(client.getClient_id()).getName());
	}

	@Test
	void Product_saveProduct_NotNull() {
		Product product = createNewProduct1();
		Assertions.assertNotNull(productService.findProductById(product.getProduct_id()));
	}

	@Test
	void Product_saveProduct_VerifyName() {
		Product product = createNewProduct1();
		Product productFound =  productService.findProductById(product.getProduct_id());
		Assertions.assertEquals(product.getName(),productFound.getName());
	}

	@Test
	void Order_saveOrder_NotNull() {
		Orders orders = createNewOrder();
		Assertions.assertNotNull(orderService.findOrderById(orders.getId()));
	}

	@Test
	void Order_saveOrder_MatchingFields() {
		Orders orders = createNewOrder();
		Orders foundOrder = orderService.findOrderById(orders.getId());
		Assertions.assertEquals(orders.getId(), foundOrder.getId());
		Assertions.assertEquals(orders.getClient().getClient_id(), foundOrder.getClient().getClient_id());
		Assertions.assertEquals(orders.getClient().getName(), foundOrder.getClient().getName());
		Assertions.assertEquals(orders.getTimestamp(), foundOrder.getTimestamp());
		Assertions.assertEquals(orders.getDetail().size(), foundOrder.getDetail().size());
		Assertions.assertEquals(orders.getDetail().get(0).getDetail_id(), foundOrder.getDetail().get(0).getDetail_id());
		Assertions.assertEquals(orders.getDetail().get(1).getDetail_id(), foundOrder.getDetail().get(1).getDetail_id());
	}

	private Client createNewClient(){
		Client client = new Client();
		client.setName("ClientOne");
		return clientService.saveClient(client);
	}

	private Orders createNewOrder(){
		Orders orders = new Orders();
		orders.setClient(createNewClient());
		orders.setDetail(createNewDetails());
		orders.setTimestamp(new Date());
		return orderService.placeOrder(orders);
	}

	private List<Detail> createNewDetails(){
		List<Detail> detailList = new ArrayList<>();

		Detail detail1 = new Detail();
		detail1.setProduct(createNewProduct1());
		detail1.setQuantity(10);
		detailList.add(detail1);

		Detail detail2 = new Detail();
		detail2.setProduct(createNewProduct2());
		detail1.setQuantity(2);
		detailList.add(detail2);
		return detailList;
	}

	private Product createNewProduct1(){
		Product product = new Product();
		product.setName("ProductOne");
		product.setPrice(10);
		return productService.saveProduct(product);
	}

	private Product createNewProduct2(){
		Product product = new Product();
		product.setName("ProductTwo");
		product.setPrice(4);
		return productService.saveProduct(product);
	}

}
